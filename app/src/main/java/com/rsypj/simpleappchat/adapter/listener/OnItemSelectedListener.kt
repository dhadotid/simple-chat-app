package com.rsypj.simpleappchat.adapter.listener

/**
 * Created by dhadotid on 19:43 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

interface OnItemSelectedListener<T> {
    fun onItemSelectedListener(data: T)
}