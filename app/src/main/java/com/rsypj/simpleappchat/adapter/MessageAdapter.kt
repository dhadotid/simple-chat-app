package com.rsypj.simpleappchat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rsypj.simpleappchat.R
import com.rsypj.simpleappchat.adapter.listener.OnItemSelectedListener
import com.rsypj.simpleappchat.adapter.viewholder.MessageViewHolder
import com.rsypj.simpleappchat.model.ChatData

/**
 * Created by dhadotid on 19:32 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

class MessageAdapter(private val userId: String, private val data: List<ChatData>,
                     private val listener: OnItemSelectedListener<ChatData>): RecyclerView.Adapter<MessageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_chat_bubble, parent, false)
        return MessageViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.setupUI(userId, data[position])
    }

    override fun getItemCount(): Int = data.size
}