package com.rsypj.simpleappchat.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.rsypj.simpleappchat.adapter.listener.OnItemSelectedListener
import com.rsypj.simpleappchat.model.ChatData
import kotlinx.android.synthetic.main.layout_chat_bubble.view.*

/**
 * Created by dhadotid on 19:33 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

class MessageViewHolder(private val view: View,
                        private val listener: OnItemSelectedListener<ChatData>): RecyclerView.ViewHolder(view) {

    init {

    }

    fun setupUI(userId: String, chatData: ChatData){
        if (chatData.uid == userId){
            view.rightWrapper.visibility = View.VISIBLE
            view.chatBubbleRight.text = chatData.message
            view.rightUsernameText.text = chatData.name
            view.setOnClickListener{
                listener.onItemSelectedListener(chatData)
            }
        }else{
            view.leftWrapper.visibility = View.VISIBLE
            view.chatBubbleLeft.text = chatData.message
            view.leftUsernameText.text = chatData.name
        }
    }
}