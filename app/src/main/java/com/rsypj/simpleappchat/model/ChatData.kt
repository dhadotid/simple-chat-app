package com.rsypj.simpleappchat.model

/**
 * Created by dhadotid on 18:48 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

data class ChatData (
    var uid: String? = "",
    var name: String? = "",
    var message: String? = "",
    var time: Long = 0
)