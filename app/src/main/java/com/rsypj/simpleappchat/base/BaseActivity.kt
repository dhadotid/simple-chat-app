package com.rsypj.simpleappchat.base

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.rsypj.simpleappchat.support.view.LoadingDialog

/**
 * Created by dhadotid on 17:34 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

abstract class BaseActivity: AppCompatActivity() {

    private var dialog: LoadingDialog? = null
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        auth = FirebaseAuth.getInstance()
        initializeDefaultValue()
    }

    fun showLoading() {
        if (dialog == null) {
            dialog = LoadingDialog(this)
        }
        dialog?.show()
    }

    fun dismissLoading() {
        dialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }



    fun showToast(message: String?){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    open fun initializeDefaultValue() {

    }

    abstract fun getLayoutId(): Int

}