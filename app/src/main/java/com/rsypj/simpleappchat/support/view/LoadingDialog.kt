package com.rsypj.simpleappchat.support.view

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.rsypj.simpleappchat.R

/**
 * Created by dhadotid on 20:59 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

class LoadingDialog(context: Context): AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            requestFeature(Window.FEATURE_NO_TITLE)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_loading)
        setCancelable(false)
    }
}
