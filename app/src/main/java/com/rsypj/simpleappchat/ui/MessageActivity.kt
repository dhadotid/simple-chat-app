package com.rsypj.simpleappchat.ui

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.rsypj.simpleappchat.support.DATABASE_NAME
import com.rsypj.simpleappchat.R
import com.rsypj.simpleappchat.adapter.MessageAdapter
import com.rsypj.simpleappchat.adapter.listener.OnItemSelectedListener
import com.rsypj.simpleappchat.base.BaseActivity
import com.rsypj.simpleappchat.model.ChatData
import kotlinx.android.synthetic.main.activity_message.*
import java.util.*

/**
 * Created by dhadotid on 18:14 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

class MessageActivity: BaseActivity() {

    private val database = Firebase.database
    private val myRef = database.getReference(DATABASE_NAME)
    private val chatList: MutableList<ChatData> = mutableListOf()
    private lateinit var userId: String
    private var userName: String? = ""
    private lateinit var adapter: MessageAdapter

    private val listener = object : OnItemSelectedListener<ChatData>{
        override fun onItemSelectedListener(data: ChatData) {

        }
    }

    override fun getLayoutId(): Int = R.layout.activity_message

    override fun initializeDefaultValue() {
        getUserDetails()
        initChatList()
        setupAdapter()
        fabSend.setOnClickListener(sendMessage)
    }

    private fun setupAdapter(){
        adapter = MessageAdapter(userId, chatList, listener)
        messageList.itemAnimator = DefaultItemAnimator()
        messageList.layoutManager = LinearLayoutManager(this)
        messageList.adapter = adapter
    }

    private val sendMessage = View.OnClickListener {
        val chatData = ChatData()
        chatData.uid = userId
        chatData.message = etMessage.text.toString()
        chatData.name = userName
        chatData.time = System.currentTimeMillis()
        myRef.child(Date().time.toString()).setValue(chatData)

        etMessage.text.clear()
        messageList.smoothScrollToPosition(chatList.size)

        adapter.notifyDataSetChanged()
    }

    private fun getUserDetails(){
        val user = auth.currentUser

        user?.let {
            userName = user.displayName
            userId = user.uid
            showToast(getString(R.string.welcome, userName))
        }
    }

    private fun initChatList() {
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                chatList.clear()
                val children = dataSnapshot?.children
                children.forEach{
                    chatList.add(it.getValue<ChatData>(ChatData::class.java)!!)
                }

                messageList.smoothScrollToPosition(chatList.size)
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                showToast("Erro load database: ${databaseError.message}")
            }
        }
        myRef.addValueEventListener(postListener)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.message_menu, menu)
        return true//super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.logout -> {
                finishAffinity()
                startActivity(Intent(this, LoginActivity::class.java))
                FirebaseAuth.getInstance().signOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}