package com.rsypj.simpleappchat.ui

import android.content.Intent
import android.view.View
import com.google.firebase.auth.UserProfileChangeRequest
import com.rsypj.simpleappchat.R
import com.rsypj.simpleappchat.base.BaseActivity
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by dhadotid on 17:47 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

class LoginActivity: BaseActivity() {

    private var isLoginActivity: Boolean = true

    override fun getLayoutId(): Int =
        R.layout.activity_login

    override fun initializeDefaultValue() {
        btnLogin.setOnClickListener(onLoginClicked)
        tvRegisterHere.setOnClickListener(onRegisterHereClicked)
    }

    private val onLoginClicked = View.OnClickListener {
        if (etEmail.text.isNullOrEmpty() && etPassword.text.isNullOrEmpty()){
            showToast("Please insert your email and password")
            return@OnClickListener
        }
        showLoading()
        if (isLoginActivity){
            loginUser(etEmail.text.toString(), etPassword.text.toString())
        }else{
            registerUser(etName.text.toString(), etEmail.text.toString(), etPassword.text.toString())
        }
    }

    private val onRegisterHereClicked = View.OnClickListener {
        if (isLoginActivity){
            isLoginActivity = false
            etName.visibility = View.VISIBLE
            btnLogin.text = getString(R.string.register)
            tvRegisterHere.text = getString(R.string.login_here)
        }else{
            isLoginActivity = true
            etName.visibility = View.GONE
            btnLogin.text = getString(R.string.login)
            tvRegisterHere.text = getString(R.string.register_here)
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if (currentUser != null)
            authenticationSuccess()
    }

    private fun registerUser(name: String, email: String, password: String){
        if (name.isNullOrEmpty()){
            dismissLoading()
            showToast(getString(R.string.please_input_your_name))
            return
        }
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful){
                    val user = auth.currentUser
                    val profileUpdates = UserProfileChangeRequest.Builder()
                        .setDisplayName(name)
                        .build()

                    user?.updateProfile(profileUpdates)
                        ?.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                authenticationSuccess()
                            }
                        }
                }else{
                    dismissLoading()
                    showToast("Authentication failed: ${task.exception?.message}")
                }
            }
    }

    private fun loginUser(email: String, password: String){
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this){ task ->
                if (task.isSuccessful){
                    authenticationSuccess()
                }else {
                    dismissLoading()
                    showToast("Authentication failed: ${task.exception?.message}")
                }
            }
    }

    private fun authenticationSuccess(){
        dismissLoading()
        startActivity(Intent(this, MessageActivity::class.java))
    }
}