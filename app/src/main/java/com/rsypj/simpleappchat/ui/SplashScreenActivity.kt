package com.rsypj.simpleappchat.ui

import android.content.Intent
import com.rsypj.simpleappchat.R
import com.rsypj.simpleappchat.base.BaseActivity

/**
 * Created by dhadotid on 20:36 | 2020-04-09.
 * -> ✉ : yudhapurbajagad@gmail.com <-
 */

class SplashScreenActivity: BaseActivity() {

    override fun getLayoutId(): Int = R.layout.activity_splash_screen

    override fun initializeDefaultValue() {
        counterToNextActivity()
    }

    private fun counterToNextActivity(){
        val thread = Thread(Runnable {
            try {
                Thread.sleep(2000)
            }catch (e: Exception){
                e.printStackTrace()
            }finally {
                finish()
                val currentUser = auth.currentUser
                if (currentUser != null){
                    startActivity(Intent(this, MessageActivity::class.java))
                }else{
                    startActivity(Intent(this, LoginActivity::class.java))
                }
            }
        })
        thread.start()
    }
}